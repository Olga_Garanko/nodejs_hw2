const { Router } = require('express');
const router = Router();
const { getUser, deleteUser, changePass } = require('../controllers/userController');

router.get('/me', getUser);
router.delete('/me', deleteUser);
router.patch('/me', changePass);

module.exports = router;