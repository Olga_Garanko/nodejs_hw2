const { Router } = require('express');
const router = Router();
const { PORT } = require('../config');

router.get('/', (req, res) => {
    res.render('index', {
        title: 'Home',
        PORT
    });
});

router.get('/login', (req, res) => {
    res.render('login', {
        title: 'Login',
        PORT
    });
});

router.get('/register', (req, res) => {
    res.render('register', {
        title: 'Registration',
        PORT
    });
});

router.get('/profile', (req, res) => {
    res.render('user', {
        title: 'Profile',
        PORT
    });
});

router.get('/profile-edit', (req, res) => {
    res.render('editUser', {
        title: 'Edit profile',
        PORT
    });
});

router.get('/notes', (req, res) => {
    if (req.query.id) {
        res.render('note', {
            title: 'Note',
            PORT
        });        
    } else {
        res.render('notes', {
            title: 'Notes',
            PORT
        });        
    }
});
router.get('/note-edit', (req, res) => {
    res.render('editNote', {
        title: 'Edit Note',
        PORT
    });      
});
router.get('/note-create', (req, res) => {
    res.render('createNote', {
        title: 'Create Note',
        PORT
    });      
});

module.exports = router;