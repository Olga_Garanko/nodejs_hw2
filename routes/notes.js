const { Router } = require('express');
const router = Router();
const { getNotes, addNote, getNote, deleteNote, updateNoteText, changeNoteCompleteness } = require('../controllers/noteController');

router.get('/', getNotes);
router.post('/', addNote);
router.get('/:id', getNote);
router.delete('/:id', deleteNote);
router.put('/:id', updateNoteText);
router.patch('/:id', changeNoteCompleteness);

module.exports = router;