const { v4: uuidv4 } = require('uuid');
let { notes } = require('../data');
const { jwtSecret } = require('../config');
const jwt = require('jsonwebtoken');

class Note {
    constructor(text, user) {
        this._id = uuidv4();
        this.userId = user._id;
        this.completed = false;
        this.text = text;
        this.createdDate = new Date();
    }
    save() {
        notes.push(this); 
    }
    static getUserNotes(user) {
        return notes.filter(item => item.userId == user._id);
    }
    static getNote(id, user) {
        return Note.getUserNotes(user).find(item => item._id == id);
    }
    static findNoteIndex(id, user) {
        return notes.findIndex(item => item._id == id && item.userId == user._id);
    }
    static deleteNote(noteIndex) {
        notes.splice(noteIndex, 1);
    }
    static updateNoteText(noteIndex, text) {
        notes[noteIndex] = {
            ...notes[noteIndex],
            text
        };
    }
    static changeNoteCompleteness(noteIndex) {
        notes[noteIndex].completed = !notes[noteIndex].completed;
    }
}
module.exports = Note;