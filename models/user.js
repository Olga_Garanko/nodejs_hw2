const { v4: uuidv4 } = require('uuid');
let { users } = require('../data');

class User {
    constructor(username, password) {
        this._id = uuidv4();
        this.username = username;
        this.password = password;
        this.createdDate = new Date();
    }
    save() {
        users.push(this); 
    }
    static getUser(user) {
        return {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate,
        };
    }
    static findUserByName(username) {
        return users.find(item => item.username == username);
    }
    static findUserByCredential({username, password}) {
        return users.find(item => item.username == username && item.password == password);
    }
    static findUserById(user) {
        return users.find(item => item._id == user._id);
    }
    static findUserIndex(user) {
        return users.findIndex(item => item._id == user._id);
    }
    static update(userIndex, password) {
        users[userIndex].password = password;
    }
    static delete(userIndex) {
        users.splice(userIndex, 1);
    }
}
module.exports = User;