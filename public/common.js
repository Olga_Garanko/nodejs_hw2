document.addEventListener('DOMContentLoaded', () => {
    if (token) {
        getUser(token);
    }

    const logoutLink = document.querySelector('.logout');
    if (logoutLink) {
        logoutLink.addEventListener('click', (e) => {
            e.preventDefault();
            logout();
            location.href = './';
        })
    }

    const registerForm = document.forms.register;
    if (registerForm) {
        registerForm.addEventListener('submit', (e) => {
            e.preventDefault();
            const res = {
                username: registerForm.elements.username.value,
                password: registerForm.elements.password.value
            }
            registration(res);
        });        
    }

    const loginForm = document.forms.login;
    if (loginForm) {
        loginForm.addEventListener('submit', (e) => {
            e.preventDefault();
            const res = {
                username: loginForm.elements.username.value,
                password: loginForm.elements.password.value
            }
            authorization(res);
        });        
    }

    const userForm = document.forms.user;
    if (userForm) {
        userForm.addEventListener('submit', (e) => {
            e.preventDefault();
            const res = {
                oldPassword: userForm.elements.oldPassword.value,
                newPassword: userForm.elements.newPassword.value
            }
            changePassword(res);
        });        
    }

    const deleteAccountLink = document.querySelector('.delete-account');
    if (deleteAccountLink) {
        deleteAccountLink.addEventListener('click', (e) => {
            e.preventDefault();
            deleteAccount();
        })
    }
})