let token = localStorage.getItem('token');
let user = JSON.parse(localStorage.getItem('user'));
const baseURL = 'http://localhost';
let PORT;

document.addEventListener('DOMContentLoaded', () => {
    const portInput = document.querySelector('#port');
    PORT = portInput.value;
})

const callApi = (fetchApi) => {
    return new Promise((resolve, reject) => {
        fetchApi
        .then(response => {
            if (response.status < 400) {
                response.json()
                .then(data => {
                    resolve(data)
                })
            } else {
                response.json()
                .then(error => {
                    reject(error);
                })
            }
        })
        .catch(err => reject(err));
    });
}

const showError = (err) => {
    const error = document.querySelector('.error');
    error.innerHTML = `${err.message}`;
}

// auth
const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
}

const authorization = (res) => {
    callApi(fetch(`${baseURL}:${PORT}/api/auth/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(res)
    }))
    .then(data => {
        localStorage.setItem('token', data.jwt_token);
        location.href = './';
    })
    .catch(err => {
        showError(err);
    });    
}

const registration = (res) => {
    callApi(fetch(`${baseURL}:${PORT}/api/auth/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(res)
    }))
    .then(data => {
        authorization(res);
    })
    .catch(err => {
        showError(err);
    });
}

// user
const getUser = (token) => {
    callApi(fetch(`${baseURL}:${PORT}/api/users/me`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(profile => {
        localStorage.setItem('user', JSON.stringify(profile));
        const user = document.querySelector('.user');
        user.innerHTML = `User ${profile.username}`;
        const privateNavs = [...document.querySelectorAll('.private')];
        privateNavs.forEach(i => {
            i.style.display = 'block';
        });
        const publicNavs = [...document.querySelectorAll('.public')];
        publicNavs.forEach(i => {
            i.style.display = 'none';
        })
    })
    .catch(() => {
        logout();
    })
}

const changePassword = (res) => {
    callApi(fetch(`${baseURL}:${PORT}/api/users/me`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(res)
    }))
    .then(() => {
        logout();
        authorization({username: user.username, password: res.newPassword});
    })
    .catch(err => {
        showError(err);
    });    
}

const deleteAccount = () => {
    callApi(fetch(`${baseURL}:${PORT}/api/users/me`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(() => {
        logout();
        location.href = './';
    })
    .catch(err => {
        console.log('error', err)
    });    
}

// notes
const changeNoteCompleteness = (id) => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes/${id}`, {
        method: 'PATCH',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(data => {
        console.log('data', data);
    })
    .catch(err => {
        console.log('error', err)
    });    
}

const deleteNote = (id) => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(() => {
        location.href = `./notes`
    })
    .catch(err => {
        console.log('error', err)
    });  
}

const createNote = (res) => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(res)
    }))
    .then(() => {
        location.href = `./notes`
    })
    .catch(err => {
        console.log('error', err)
    });  
}

const editNote = (id, res) => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(res)
    }))
    .then(() => {
        location.href = `./notes?id=${id}`
    })
    .catch(err => {
        console.log('error', err)
    });  
}

const getNotes = () => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(notes => {
        renderNotes(notes);
    })
    .catch(err => {
        console.log('error', err)
    });
}

const getNote = (id) => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes/${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(data => {
        renderNote(id, data.note);
    })
    .catch(err => {
        showError(err);
    });
}

const getNoteData = (id) => {
    callApi(fetch(`${baseURL}:${PORT}/api/notes/${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }))
    .then(data => {
        renderEditNote(id, data.note)
    })
    .catch(err => {
        showError(err);
    });
}

// render
const notesItem = (item) => {
    return `
        <tr data-note="${item._id}">
            <td>${item._id}</td>
            <td><a href="#" class="view-note">${item.text}</a></td>
            <td>
                <label>
                    <input type="checkbox" ${item.completed ? 'checked' : ''} class="completed" />
                    <span></span>
                </label>
            </td>
            <td>
                <a href="#" class="edit-note">Edit</a>
                <a href="#" class="delete-note" style="color: red"> &times;</a>
            </td>
        </tr>
    `
}

const renderNotes = (notes) => {
    const table = document.querySelector('.notes');
    let content = '';        
    notes.notes.forEach(i => {
        content += notesItem(i);
    })
    table.innerHTML = content;
    const checkboxes = [...document.querySelectorAll('.completed')];
    checkboxes.forEach(i => {
        i.addEventListener('change', (e) => {
            changeNoteCompleteness(e.target.closest('tr').dataset.note)
        })
    })
    const editItems = [...document.querySelectorAll('.edit-note')];
    editItems.forEach(i => {
        i.addEventListener('click', (e) => {
            e.preventDefault();
            const id = e.target.closest('tr').dataset.note;
            location.href = `./note-edit?id=${id}`
        })
    })
    const viewItems = [...document.querySelectorAll('.view-note')];
    viewItems.forEach(i => {
        i.addEventListener('click', (e) => {
            e.preventDefault();
            const id = e.target.closest('tr').dataset.note;
            location.href = `./notes?id=${id}`
        })
    })
    const deleteItems = [...document.querySelectorAll('.delete-note')];
    deleteItems.forEach(i => {
        i.addEventListener('click', (e) => {
            e.preventDefault();
            const id = e.target.closest('tr').dataset.note;
            deleteNote(id)
        })
    })
}

const renderNote = (id, note) => {
    const noteContent = `
        <p><b>NoteID:</b> ${note._id}</p>
        <p><b>Text:</b> ${note.text}</p>
        <p><b>Completed:</b> ${note.completed}</p>
        <p><b>Created Date:</b> ${note.createdDate}</p>
        <p><a href="./note-edit?id=${id}">Edit note</a></p>
    `;
    const noteDiv = document.querySelector('.note');
    noteDiv.innerHTML = noteContent;
}

const renderEditNote = (id, note) => {
    const noteContent = `
        <label for="text">Text</label>
        <input type="text" value="${note.text}" name="text" id="text" required />
        <button type="submit" class="btn edit">Send</button>
    `;
    const noteDiv = document.querySelector('.note');
    noteDiv.innerHTML = noteContent;
    const editNoteForm = document.querySelector('.note');
    editNoteForm.addEventListener('submit', (e) => {
        e.preventDefault();
        const res = {
            text: editNoteForm.elements.text.value
        }
        editNote(id, res);
    })
}