const User = require('../models/user');
const { jwtSecret } = require('../config');
const jwt = require('jsonwebtoken');

module.exports.register = (req, res) => {
    const { username, password } = req.body;
    if (!username) {
        return res.status(400).json({ message: 'No username in body found'})
    }
    if (!password) {
        return res.status(400).json({ message: 'No password in body found'})
    }
    const user = User.findUserByName(username);
    if (user) {
        return res.status(400).json({ message: 'User already exist'})
    }
    if (!user) {
        const userModel = new User(username, password);
        try {
            userModel.save();
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.status(200).json({ message: 'Success'}); 
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

module.exports.login = (req, res) => {
    const { username, password } = req.body;
    if (!username) {
        return res.status(400).json({ message: 'No username in body found'})
    }
    if (!password) {
        return res.status(400).json({ message: 'No password in body found'})
    }
    const user = User.findUserByCredential({ username, password });
    if (!user) {
        return res.status(400).json({ message: 'Invalid login or password'})
    }
    if (user) {
        let token 
        try {
            token = jwt.sign(JSON.stringify(user), jwtSecret);
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.json({ jwt_token: token });
    }
    res.status(500).json({ message: 'Internal Server Error' });
};