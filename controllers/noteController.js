const Note = require('../models/note');

const getNotes = (req, res) => {
    let notes;
    try {
        notes = Note.getUserNotes(req.user);
    } catch(err) {
        return res.status(500).json({ message: `${err.message}`});
    }
    res.status(200).json({ notes });
};

const getNote = (req, res) => {
    const note = Note.getNote(req.params.id, req.user);
    if (!note) {
        return res.status(400).json({ message: 'There is no note with such id'})
    }
    if (note) {
        return res.status(200).json({ note });
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

const deleteNote = (req, res) => {
    const noteIndex = Note.findNoteIndex(req.params.id, req.user);
    if (noteIndex < 0) {
        return res.status(400).json({ message: 'There is no such note'})
    }
    if (noteIndex >= 0) {
        try {
            Note.deleteNote(noteIndex);
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.status(200).json({ message: 'Success'});
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

const addNote = (req, res) => {
    const { text } = req.body;
    if (!text) {
        return res.status(400).json({ message: 'Text is obligatory field'})
    }
    if (text) {
        const noteModel = new Note(text, req.user);
        try {
            noteModel.save();
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.status(200).json({ message: 'Success'}); 
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

const updateNoteText = (req, res) => {
    const { text } = req.body;
    if (!text) {
        return res.status(400).json({ message: 'Text is obligatory field'})
    }
    if (text) {
        const noteIndex = Note.findNoteIndex(req.params.id, req.user);
        if (noteIndex < 0) {
            return res.status(400).json({ message: 'There is no such note'})
        }
        if (noteIndex >= 0) {
            try {
                Note.updateNoteText(noteIndex, text);
            } catch(err) {
                return res.status(500).json({ message: `${err.message}`});
            }
            return res.status(200).json({ message: 'Success'}); 
        }  
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

const changeNoteCompleteness = (req, res) => {
    const noteIndex = Note.findNoteIndex(req.params.id, req.user);
    if (noteIndex < 0) {
        return res.status(400).json({ message: 'There is no such note'})
    }
    if (noteIndex >= 0) {
        try {
           Note.changeNoteCompleteness(noteIndex); 
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.status(200).json({ message: 'Success'});
    }
    res.status(500).json({ message: 'Internal Server Error' });    
};

module.exports = {
    getNotes,
    getNote,
    deleteNote,
    addNote,
    updateNoteText,
    changeNoteCompleteness
}