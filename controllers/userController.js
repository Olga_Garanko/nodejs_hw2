const User = require('../models/user');

module.exports.getUser = (req, res) => {
    const user = User.findUserById(req.user);
    if (!user) {
        return res.status(400).json({ message: 'There is no such user'})
    }
    if (user) {
        let userProfile;
        try {
            userProfile = User.getUser(user);
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.status(200).json(userProfile);
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

module.exports.deleteUser = (req, res) => {
    const userIndex = User.findUserIndex(req.user);
    if (userIndex < 0) {
        return res.status(400).json({ message: 'There is no such user'})
    }
    if (userIndex >= 0) {
        try {
            User.delete(userIndex);
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        res.status(200).json({ message: 'Success' });
    }
    res.status(500).json({ message: 'Internal Server Error' });
};

module.exports.changePass = (req, res) => {
    const { oldPassword, newPassword } = req.body;
    if (!oldPassword) {
        return res.status(400).json({ message: 'No oldPassword in body found'})
    }
    if (!newPassword) {
        return res.status(400).json({ message: 'No newPassword in body found'})
    }
    const userIndex = User.findUserIndex(req.user);
    if (userIndex < 0) {
        return res.status(400).json({ message: 'There is no such user'})
    }
    if (userIndex >= 0 && req.user.password != oldPassword) {
        return res.status(400).json({ message: 'Invalid old password'})
    }
    if (userIndex >= 0 && req.user.password == oldPassword) {
        try {
            User.update(userIndex, newPassword);
        } catch(err) {
            return res.status(500).json({ message: `${err.message}`});
        }
        return res.status(200).json({ message: 'Success'});
    }
    res.status(500).json({ message: 'Internal Server Error' });
};