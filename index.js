const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const { PORT } = require('./config');
const uiRoutes = require('./routes/ui');
const usersRoutes = require('./routes/users');
const notesRoutes = require('./routes/notes');
const authRoutes = require('./routes/auth');

const authMiddleware = require('./authMiddleware');

const app = express();

const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());
app.use('/', uiRoutes);
app.use('/api/auth', authRoutes);

app.use(authMiddleware);

app.use('/api/users', usersRoutes);
app.use('/api/notes', notesRoutes);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});